(*
 * @Title       : Program Nota Seafood
 * @Matkul		: Struktur Data
 * @CreatedBy	: Kelompok 11
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

{ COMMENT 
 1. Disini kita akan membuat sebuah program yang bernama nota_seafood
}
program nota_seafood;
uses crt;

{ COMMENT
 2. Lalu yang pertama kita lakukan adalah mendeklarasikan atau membuat 5 buah variable yang akan digunakan untuk proses pembuatan program notanya. 
	a.Variable  menu_id,portion mengunakan tipe data integer.
	b.Sedangkan variable price mengunakan longint
	b.Variable menu mengunakan string sebanyak 50 karakter dan variable cust_name sebanyak 100 karakter
}
var 
  menu_id,
  portion : integer;
  
  price   : longint;
  
  menu 		: string[50];
  cust_name : string[100]; 
 
 { COMMENT
  3. Lalu setelah mendeklarasikan variable kita buat sebuah procedure untuk membuat nilai nama menu dan harga yang harus dibayarkan 
	 berdasarkan pilihan menu idnya 
 }
 procedure get( value : integer );
 begin 
	case ( value ) of 
	1 : 
		begin 
			menu  := 'Udang saus tiram';
			price := 20000 * portion;
		end;
	2 : 
		begin 
			menu  := 'Ikan bakar';
			price := 10000 * portion;
		end;
	3 : 
		begin 
			menu  := 'Cumi';
			price := 5000 * portion;
		end;
	4 : 
		begin
			menu  := 'Ikan kakap bakar';
			price := 30000 * portion;
		end;
	5 : 
		begin 
			menu  := 'Nasi putih';
			price := 4000 * portion;
		end;
	6 : 
		begin 
			menu  := 'Lele goreng';
			price := 13000 * portion;
		end;
		
		otherwise 
			menu  := 'Mohon maaf,menu yang ada pilih belum tersedia';
	end;		
 end; 


begin 
	clrscr;
	
	writeln('|  Daftar menu makanan seafood  |');
	writeln('=================================');
	writeln('| No |       Menu       | Price |');
	writeln('---------------------------------');
	writeln('| 1  | Udang saus tiram | 20000 |');
	writeln('| 2  | Ikan bakar       | 10000 |');
	writeln('| 3  | Cumi             | 5000  |');
	writeln('| 4  | Ikan kakap bakar | 30000 |');
	writeln('| 5  | Nasi putih       | 4000  |');
	writeln('| 6  | Lele goreng      | 13000 |');
	writeln('---------------------------------');
	
	writeln('');
	
	{ COMMENT 
	 4.Lalu masukan data inputnya 
	}
	
	{ COMMENT
		RUN FREE PASCALNYA 
	}
	writeln('--------- Input Data --------');	
	
	writeln('');
	
	write('Masukan nama customer         : ');
	readln(cust_name);
	
	write('Masukan id menu [1|2|3|4|5|6] : ');
	readln(menu_id);
	
	write('Masukan jumlah porsi          : ');
	readln(portion);
	
	writeln('');
	
	writeln('--------- Output Data --------');		
	
	writeln('');
	
	get(menu_id);
	
	writeln('Nama customer   : ',cust_name);
	writeln('Nama menu       : ',menu); 
	writeln('Banyak porsi    : ',portion);
	writeln('Total harga Rp. : ',price);
	
	writeln('Dibayarkan  Rp. : ',price);
end.
  